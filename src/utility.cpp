#include <iostream>

#include "TcpLayer.h"
#include "UdpLayer.h"
#include "IPv4Layer.h"
#include "IPv6Layer.h"
#include "Packet.h"

#include "utility.h"

bool Mahsan::Utility::OpenPcapFile(pcpp::IFileReaderDevice *const pcapFile)
{
    bool status;
    if (NULL == pcapFile || !pcapFile->open())
        status = false;
    else
        status = true;
    return status;
}

Mahsan::TransportType Mahsan::Utility::GetTransportType(pcpp::Packet const &parsedPacket)
{
    Mahsan::TransportType type;
    pcpp::TcpLayer *tcpLayer = parsedPacket.getLayerOfType<pcpp::TcpLayer>();
    if (parsedPacket.getLayerOfType<pcpp::TcpLayer>())
        type = Mahsan::TransportType::TCP;
    else if (parsedPacket.getLayerOfType<pcpp::UdpLayer>())
        type = Mahsan::TransportType::UDP;
    else
        type = Mahsan::TransportType::NONE;
    return type;
}

void Mahsan::Utility::GetEndPointIPs(pcpp::Packet const &parsedPacket, std::string &srcIP, std::string &dstIP)
{
    if (parsedPacket.isPacketOfType(pcpp::IPv4))
    {
        srcIP = parsedPacket.getLayerOfType<pcpp::IPv4Layer>()->getSrcIPv4Address().toString();
        dstIP = parsedPacket.getLayerOfType<pcpp::IPv4Layer>()->getDstIPv4Address().toString();
    }
    else if (parsedPacket.isPacketOfType(pcpp::IPv6))
    {
        srcIP = parsedPacket.getLayerOfType<pcpp::IPv6Layer>()->getSrcIPv6Address().toString();
        dstIP = parsedPacket.getLayerOfType<pcpp::IPv6Layer>()->getDstIPv6Address().toString();
    }
    else
    {
        srcIP = "Invalid";
        dstIP = "Invalid";
    }
}

void Mahsan::Utility::GetEndPointPorts(pcpp::Packet const &parsedPacket, std::string &srcPort, std::string &dstPort)
{
    if (Mahsan::Utility::GetTransportType(parsedPacket) == Mahsan::TransportType::TCP)
    {
        pcpp::TcpLayer *tcpLayer = parsedPacket.getLayerOfType<pcpp::TcpLayer>();
        srcPort = std::to_string(tcpLayer->getSrcPort());
        dstPort = std::to_string(tcpLayer->getDstPort());
    }
    else if (Mahsan::Utility::GetTransportType(parsedPacket) == Mahsan::TransportType::UDP)
    {
        pcpp::UdpLayer *udpLayer = parsedPacket.getLayerOfType<pcpp::UdpLayer>();
        srcPort = std::to_string(udpLayer->getSrcPort());
        dstPort = std::to_string(udpLayer->getDstPort());
    }
    else
    {
        srcPort = "Invalid";
        dstPort = "Invalid";
    }
}

std::string Mahsan::Utility::TrasnportTypeToString(Mahsan::TransportType e)
{
    switch (e)
    {
    case Mahsan::TransportType::TCP:
        return "TCP";
    case Mahsan::TransportType::UDP:
        return "UDP";
    case Mahsan::TransportType::NONE:
        return "NOT TCP NOR UDP";
    default:
        return "NOT TCP NOR UDP";
    }
}