#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

#include "transportanalyzer.h"
#include "ipanalyzer.h"
#include "ipanalyzerinterface.h"
#include "analyzerinterface.h"

void AnalyzeIP(std::string const &address);
void AnalyzeTransport(std::string const &address);

int main(int argc, char *argv[])
{
    std::string address("./tests/sample.pcap");
    AnalyzeIP(address);
    AnalyzeTransport(address);
    return 0;
}

void AnalyzeIP(std::string const &address)
{
    Mahsan::IPAnalyzerInterface *IpAnalyzer = new Mahsan::IPAnalyzer(address);
    std::vector<std::string> ipAddresses;
    std::unordered_map<std::string, int> ipTraffic;
    std::unordered_map<std::string, int> ipTrasnportTraffic;
    IpAnalyzer->getAddresses(ipAddresses);
    IpAnalyzer->getTrafficPerEndPoint(ipTraffic);
    IpAnalyzer->getTransportTypeCount(ipTrasnportTraffic);

    std::cout << "IP Addresses:"
              << "\n";
    for (auto &addr : ipAddresses)
    {
        std::cout << "address is: " << addr << "\n";
    }

    std::cout << "IP Traffic:"
              << "\n";
    for (auto &tf : ipTraffic)
    {
        std::cout << "address is: " << tf.first << " count of packets: " << tf.second << "\n";
    }

    std::cout << "Count of Trasport Traffic For IP Layer:"
              << "\n";
    for (auto &tf : ipTrasnportTraffic)
    {
        std::cout << "Type is: " << tf.first << " count of packets: " << tf.second << "\n";
    }
}

void AnalyzeTransport(std::string const &address)
{
    Mahsan::AnalyzerInterFace *TransAnalyzer = new Mahsan::Transportanalyzer(address);
    std::vector<std::string> tpAddresses;
    std::unordered_map<std::string, int> tpTraffic;
    TransAnalyzer->getAddresses(tpAddresses);
    TransAnalyzer->getTrafficPerEndPoint(tpTraffic);

    std::cout << "Trasport Addresses:"
              << "\n";
    for (auto &addr : tpAddresses)
    {
        std::cout << "address is: " << addr << "\n";
    }

    std::cout << "Trasport Traffic:"
              << "\n";
    for (auto &tf : tpTraffic)
    {
        std::cout << "address is: " << tf.first << " count of packets: " << tf.second << "\n";
    }
}