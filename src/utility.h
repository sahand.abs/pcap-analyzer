#ifndef UTILITY
#define UTILITY

#include <string>
#include <unordered_map>

#include "PcapFileDevice.h"

namespace Mahsan
{

    enum TransportType
    {
        TCP = 0,
        UDP,
        NONE
    };

    namespace Utility
    {
        template <typename T>
        void UpdateAnalyzer(std::unordered_map<T, int> &trafficMap, T const &key)
        {
            trafficMap.find(key) == trafficMap.end() ? trafficMap[key] = 1 : trafficMap[key]++;
        }
        void GetEndPointPorts(pcpp::Packet const &parsedPacket, std::string &srcPort, std::string &dstPort);
        void GetEndPointIPs(pcpp::Packet const &parsedPacket, std::string &srcIP, std::string &dstIP);
        bool OpenPcapFile(pcpp::IFileReaderDevice *const pcapFile);
        Mahsan::TransportType GetTransportType(pcpp::Packet const &parsedPacket);
        std::string TrasnportTypeToString(Mahsan::TransportType e);

    } // namespace Utility

} // namespace Mahsan

#endif