#ifndef ANALYZER_INTERFACE
#define ANALYZER_INTERFACE

#include <unordered_map>
#include <vector>

namespace Mahsan
{

    class AnalyzerInterFace
    {
    public:
        virtual void getAddresses(std::vector<std::string> &addresses) const = 0;
        virtual void getTrafficPerEndPoint(std::unordered_map<std::string, int> &trafficPerAddress) const = 0;
    };

} // namespace Mahsan
#endif