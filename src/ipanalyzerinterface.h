#ifndef IP_ANALYZER_INTERFACE
#define IP_ANALYZER_INTERFACE

#include <unordered_map>
#include <vector>

#include "analyzerinterface.h"

namespace Mahsan
{

    class IPAnalyzerInterface : public AnalyzerInterFace
    {
    public:
        virtual void getTransportTypeCount(std::unordered_map<std::string, int> &trafficPerAddress) const = 0;
    };

} // namespace Mahsan
#endif