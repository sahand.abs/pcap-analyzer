#ifndef TRASNPORT_ANALYZER
#define TRASNPORT_ANALYZER

#include "PcapFileDevice.h"

#include "analyzerinterface.h"

namespace Mahsan
{

    class Transportanalyzer : public AnalyzerInterFace
    {
    public:
        Transportanalyzer(std::string const &pcapAddress);
        virtual void getAddresses(std::vector<std::string> &addresses) const override;
        virtual void getTrafficPerEndPoint(std::unordered_map<std::string, int> &trafficPerAddress) const override;

    private:
        void init(std::string const &pcapAddress);

    protected:
        std::unordered_map<std::string, int> trafficMap;
    };

} // namespace Mahsan
#endif