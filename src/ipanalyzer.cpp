#include <iostream>

#include "Packet.h"

#include "ipanalyzer.h"
#include "utility.h"

using namespace Mahsan;

IPAnalyzer::IPAnalyzer(std::string const &pcapAddress)
{
    try
    {
        init(pcapAddress);
    }

    catch (const std::exception &e)
    {
        std::cerr << "could not construct IP anlyzer object, error at:" << e.what() << std::endl;
        throw e;
    }
}

void IPAnalyzer::init(std::string const &pcapAddress)
{
    pcpp::IFileReaderDevice *pcapFile = pcpp::IFileReaderDevice::getReader(pcapAddress);

    if (!Mahsan::Utility::OpenPcapFile(pcapFile))
    {
        std::cerr << "Cannot determine reader for file type" << std::endl;
        throw std::runtime_error("Problem with the PCAP file");
    }

    pcpp::RawPacket rawPacket;

    while (pcapFile->getNextPacket(rawPacket))
    {
        try
        {

            pcpp::Packet parsedPacket(&rawPacket);

            std::string keySrc;
            std::string keyDst;

            Mahsan::Utility::GetEndPointIPs(parsedPacket, keySrc, keyDst);

            auto key = keySrc + "->" + keyDst;

            Mahsan::Utility::UpdateAnalyzer<std::string>(trafficMap, key);

            auto transportType = Mahsan::Utility::GetTransportType(parsedPacket);

            Mahsan::Utility::UpdateAnalyzer<Mahsan::TransportType>(transportMap, transportType);
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << '\n';
            // throw(e);
        }
    }
    pcapFile->close();
    delete pcapFile;
}

void IPAnalyzer::getAddresses(std::vector<std::string> &addresses) const
{
    for (auto &key : trafficMap)
    {
        addresses.push_back(key.first);
    }
}

void IPAnalyzer::getTrafficPerEndPoint(std::unordered_map<std::string, int> &trafficPerAddress) const
{
    trafficPerAddress = trafficMap;
}

void IPAnalyzer::getTransportTypeCount(std::unordered_map<std::string, int> &trafficPerAddress) const
{
    for (auto &trasportTraffic : transportMap)
    {
        trafficPerAddress[Mahsan::Utility::TrasnportTypeToString(trasportTraffic.first)] = trasportTraffic.second;
    }
}
