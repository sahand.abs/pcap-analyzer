#ifndef IP_ANALYZER
#define IP_ANALYZER

#include "ipanalyzerinterface.h"
#include "utility.h"

namespace Mahsan
{
    class IPAnalyzer : public IPAnalyzerInterface
    {
    public:
        IPAnalyzer(std::string const &pcapAddress);
        virtual void getAddresses(std::vector<std::string> &addresses) const override;
        virtual void getTrafficPerEndPoint(std::unordered_map<std::string, int> &trafficPerAddress) const override;
        virtual void getTransportTypeCount(std::unordered_map<std::string, int> &trafficPerAddress) const override;

    private:
        void init(std::string const &pcapAddress);

    protected:
        std::unordered_map<std::string, int> trafficMap;
        std::unordered_map<Mahsan::TransportType, int> transportMap;
    };

} // namespace Mahsan
#endif