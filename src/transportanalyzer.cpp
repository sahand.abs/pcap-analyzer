#include <iostream>

#include "Packet.h"

#include "transportanalyzer.h"
#include "utility.h"

using namespace Mahsan;

Transportanalyzer::Transportanalyzer(std::string const &pcapAddress)
{
    try
    {
        init(pcapAddress);
    }

    catch (const std::exception &e)
    {
        std::cerr << "could not construct transport anlyzer object, error at:" << e.what() << std::endl;
        throw e;
    }
}

void Transportanalyzer::init(std::string const &pcapAddress)
{
    pcpp::IFileReaderDevice *pcapFile = pcpp::IFileReaderDevice::getReader(pcapAddress);

    if (!Mahsan::Utility::OpenPcapFile(pcapFile))
    {
        std::cerr << "Cannot determine reader for file type" << std::endl;
        throw std::runtime_error("Problem with the PCAP file");
    }

    pcpp::RawPacket rawPacket;

    while (pcapFile->getNextPacket(rawPacket))
    {
        try
        {

            pcpp::Packet parsedPacket(&rawPacket);

            std::string fistKeyPartSrc;
            std::string fistKeyPartDst;
            std::string secondKeyPartSrc;
            std::string secondKeyPartDst;

            Mahsan::Utility::GetEndPointIPs(parsedPacket, fistKeyPartSrc, fistKeyPartDst);
            Mahsan::Utility::GetEndPointPorts(parsedPacket, secondKeyPartSrc, secondKeyPartDst);

            auto srcKey = fistKeyPartSrc + ":" + secondKeyPartSrc;
            auto dstKey = fistKeyPartDst + ":" + secondKeyPartDst;
            auto key = srcKey + "->" + dstKey;

            Mahsan::Utility::UpdateAnalyzer<std::string>(trafficMap, key);
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << '\n';
            // throw(e);
        }
    }
    pcapFile->close();
    delete pcapFile;
}

void Transportanalyzer::getAddresses(std::vector<std::string> &addresses) const
{
    for (auto &key : trafficMap)
    {
        addresses.push_back(key.first);
    }
}

void Transportanalyzer::getTrafficPerEndPoint(std::unordered_map<std::string, int> &trafficPerAddress) const
{
    trafficPerAddress = trafficMap;
}