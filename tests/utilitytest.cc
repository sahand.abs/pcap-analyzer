#include <unordered_map>

#include <gtest/gtest.h>
#include "Packet.h"

#include "utility.h"

class ValidPcapFileTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        std::string pcapAddress("../tests/sample.pcap");
        pcapFile = pcpp::IFileReaderDevice::getReader(pcapAddress);
        Mahsan::Utility::OpenPcapFile(pcapFile);
        pcapFile->getNextPacket(rawPacket);
        parsedPacket = &rawPacket;
    }

    void TearDown() override
    {
        delete pcapFile;
    }

    pcpp::IFileReaderDevice *pcapFile;
    pcpp::RawPacket rawPacket;
    pcpp::Packet parsedPacket;
};

TEST(UtilityTest_UpdateAnalyzer, ExistCheck)
{
    std::unordered_map<std::string, int> map;
    std::string src = "192.168.14.22:22";
    std::string dst = "192.168.14.44:78";
    auto key = src + "->" + dst;
    map[key] = 1;
    Mahsan::Utility::UpdateAnalyzer<std::string>(map, key);
    ASSERT_EQ(map[key], 2); // "Existed key did not increased"
}

TEST(UtilityTest_UpdateAnalyzer, NotExistCheck)
{
    std::unordered_map<std::string, int> map;
    std::string src = "192.168.14.22:22";
    std::string dst = "192.168.14.44:78";
    auto key = src + "->" + dst;
    Mahsan::Utility::UpdateAnalyzer<std::string>(map, key);
    ASSERT_EQ(map[key], 1); // "Not-existed key does not qeual to 1"
}

TEST(UtilityTest_UpdateAnalyzer, EmptyCheck)
{
    std::unordered_map<std::string, int> map;
    std::string src;
    std::string dst;
    auto key = src + "->" + dst;
    Mahsan::Utility::UpdateAnalyzer<std::string>(map, key);
    ASSERT_EQ(map[key], 1); // "Empty check Error"
}

TEST(UtilityTest_UpdateAnalyzerWithTrasportLayar, ExistCheckTCP)
{
    std::unordered_map<Mahsan::TransportType, int> map;
    Mahsan::TransportType type = Mahsan::TransportType::TCP;
    map[type] = 1;
    Mahsan::Utility::UpdateAnalyzer<Mahsan::TransportType>(map, type);
    ASSERT_EQ(map[type], 2); // "Existed key did not increased"
}

TEST(UtilityTest_UpdateAnalyzerWithTrasportLayar, ExistCheckUDP)
{
    std::unordered_map<Mahsan::TransportType, int> map;
    Mahsan::TransportType type = Mahsan::TransportType::UDP;
    map[type] = 1;
    Mahsan::Utility::UpdateAnalyzer<Mahsan::TransportType>(map, type);
    ASSERT_EQ(map[type], 2); // "Existed key did not increased"
}

TEST(UtilityTest_UpdateAnalyzerWithTrasportLayar, NotExistCheckTCP)
{
    std::unordered_map<Mahsan::TransportType, int> map;
    Mahsan::TransportType type = Mahsan::TransportType::TCP;
    Mahsan::Utility::UpdateAnalyzer<Mahsan::TransportType>(map, type);
    ASSERT_EQ(map[type], 1); // "Not-existed key does not qeual to 1"
}

TEST(UtilityTest_UpdateAnalyzerWithTrasportLayar, NotExistCheckUDP)
{
    std::unordered_map<Mahsan::TransportType, int> map;
    Mahsan::TransportType type = Mahsan::TransportType::UDP;
    Mahsan::Utility::UpdateAnalyzer<Mahsan::TransportType>(map, type);
    ASSERT_EQ(map[type], 1); // "Not-existed key does not qeual to 1"
}

TEST(UtilityTest_TrasnportTypeToString, CheckUDP)
{
    Mahsan::TransportType type = Mahsan::TransportType::UDP;
    auto answ = Mahsan::Utility::TrasnportTypeToString(type);
    ASSERT_STREQ(answ.c_str(), "UDP"); // "UDP to str not worked corectly"
}

TEST(UtilityTest_TrasnportTypeToString, CheckTCP)
{
    Mahsan::TransportType type = Mahsan::TransportType::TCP;
    auto answ = Mahsan::Utility::TrasnportTypeToString(type);
    ASSERT_STREQ(answ.c_str(), "TCP"); // "TCP to str not worked corectly"
}

TEST(UtilityTest_TrasnportTypeToString, CheckNONE)
{
    Mahsan::TransportType type = Mahsan::TransportType::NONE;
    auto answ = Mahsan::Utility::TrasnportTypeToString(type);
    ASSERT_STREQ(answ.c_str(), "NOT TCP NOR UDP"); // "NONE to str not worked corectly"
}

TEST_F(ValidPcapFileTest, CheckValidFile)
{
    auto result = Mahsan::Utility::OpenPcapFile(pcapFile);
    ASSERT_EQ(result, true); // "check valid file did not worked corectly"
}

TEST(UtilityTest_CheckFile, CheckNotValidFile)
{
    std::string pcapAddress("NOTVALIDFILE");
    pcpp::IFileReaderDevice *pcapFile = pcpp::IFileReaderDevice::getReader(pcapAddress);
    auto result = Mahsan::Utility::OpenPcapFile(pcapFile);
    delete pcapFile;
    ASSERT_EQ(result, false); // "check not valid file did not worked corectly"
}

TEST(UtilityTest_CheckFile, CheckNull)
{
    auto result = Mahsan::Utility::OpenPcapFile(nullptr);
    ASSERT_EQ(result, false); // "check Null did not worked corectly"
}

TEST_F(ValidPcapFileTest, GetTransportTypetest)
{
    auto result = Mahsan::Utility::GetTransportType(parsedPacket);
    ASSERT_EQ(result, Mahsan::TransportType::TCP); // "GetTransportType did not worked for first test packet"
}

TEST_F(ValidPcapFileTest, GetEndPointPortsTestSrc)
{
    std::string src;
    std::string dst;
    Mahsan::Utility::GetEndPointPorts(parsedPacket, src, dst);
    ASSERT_STREQ((src).c_str(), "34515"); // "GetEndPointPorts did not worked for first test packet for src port"
}

TEST_F(ValidPcapFileTest, GetEndPointPortsTestDst)
{
    std::string src;
    std::string dst;
    Mahsan::Utility::GetEndPointPorts(parsedPacket, src, dst);
    ASSERT_STREQ((dst).c_str(), "19"); // "GetEndPointPorts did not worked for first test packet for dst port"
}

TEST_F(ValidPcapFileTest, GetEndPointIPsTestSrc)
{
    std::string src;
    std::string dst;
    Mahsan::Utility::GetEndPointIPs(parsedPacket, src, dst);
    ASSERT_STREQ((src).c_str(), "176.126.243.198"); // "GetEndPointIPs did not worked for first test packet for src IP"
}

TEST_F(ValidPcapFileTest, GetEndPointIPsTestDst)
{
    std::string src;
    std::string dst;
    Mahsan::Utility::GetEndPointIPs(parsedPacket, src, dst);
    ASSERT_STREQ((dst).c_str(), "185.47.63.113"); // "GetEndPointIPs did not worked for first test packet for dst IP"
}