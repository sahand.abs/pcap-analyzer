#include <unordered_map>

#include <gtest/gtest.h>

#include "transportanalyzer.h"

TEST(TransportAnalyzerTest, CheckAddresses)
{
    std::string pcapFileAddress("../tests/sample.pcap");
    std::vector<std::string> desiredOutput;
    desiredOutput.push_back("185.47.63.113:19->176.126.243.198:34515");
    desiredOutput.push_back("176.126.243.198:34515->185.47.63.113:19");
    std::vector<std::string> result;
    Mahsan::Transportanalyzer analyzer(pcapFileAddress);
    analyzer.getAddresses(result);
    ASSERT_TRUE(result == desiredOutput); // "Get Addresses does not work on Sample"
}

TEST(TransportAnalyzerTest, CheckTraffic)
{
    std::string pcapFileAddress("../tests/sample.pcap");
    std::unordered_map<std::string, int> desiredOutput;
    desiredOutput["185.47.63.113:19->176.126.243.198:34515"] = 12;
    desiredOutput["176.126.243.198:34515->185.47.63.113:19"] = 10;
    std::unordered_map<std::string, int> result;
    Mahsan::Transportanalyzer analyzer(pcapFileAddress);
    analyzer.getTrafficPerEndPoint(result);
    ASSERT_TRUE(result == desiredOutput); // "Get Traffic does not work on Sample"
}