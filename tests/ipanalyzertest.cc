#include <unordered_map>

#include <gtest/gtest.h>

#include "ipanalyzer.h"

TEST(IpAnalyzerTest, CheckAddresses)
{
    std::string pcapFileAddress("../tests/sample.pcap");
    std::vector<std::string> desiredOutput;
    desiredOutput.push_back("185.47.63.113->176.126.243.198");
    desiredOutput.push_back("176.126.243.198->185.47.63.113");
    std::vector<std::string> result;
    Mahsan::IPAnalyzer analyzer(pcapFileAddress);
    analyzer.getAddresses(result);
    ASSERT_TRUE(result == desiredOutput); // "Get Addresses does not work on Sample"
}

TEST(IpAnalyzerTest, CheckTraffic)
{
    std::string pcapFileAddress("../tests/sample.pcap");
    std::unordered_map<std::string, int> desiredOutput;
    desiredOutput["185.47.63.113->176.126.243.198"] = 12;
    desiredOutput["176.126.243.198->185.47.63.113"] = 10;
    std::unordered_map<std::string, int> result;
    Mahsan::IPAnalyzer analyzer(pcapFileAddress);
    analyzer.getTrafficPerEndPoint(result);
    ASSERT_TRUE(result == desiredOutput); // "Get Traffic does not work on Sample"
}

TEST(IpAnalyzerTest, CheckTransportCount)
{
    std::string pcapFileAddress("../tests/sample.pcap");
    std::unordered_map<std::string, int> desiredOutput;
    desiredOutput["TCP"] = 22;
    std::unordered_map<std::string, int> result;
    Mahsan::IPAnalyzer analyzer(pcapFileAddress);
    analyzer.getTransportTypeCount(result);
    ASSERT_TRUE(result == desiredOutput); // "Get Traffic does not work on Sample"
}